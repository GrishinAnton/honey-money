import { combineReducers } from "@reduxjs/toolkit";
import { currencyTable } from "./features/currencyTable/ducks";

const rootReducers = combineReducers({
  currencyTable
});

export default rootReducers;
