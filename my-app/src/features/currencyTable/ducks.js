import { createSlice, createAction } from "@reduxjs/toolkit";
import { all, put, takeEvery, delay } from "redux-saga/effects";
import { createSelector } from "reselect";
import * as R from "ramda";
import { defaultObj } from "../../data/currencyTable";
import { diff } from "../../utils/sortBy";

const prefix = "currencyTable";

const currencyTableData = createSlice({
  name: prefix,
  initialState: [],
  reducers: {
    setCurrencyTable: (state, action) => {
      state.push(...action.payload);
    },
    updateCurrencyFavorite: (state, action) => {
      const item = state.find(i => i.id === action.payload.id);
      item.favorite.status = action.payload.status;
      item.favorite.updatedAt = action.payload.updatedAt;
    }
  }
});

export const sortedCurrencyTableData = createSelector(
  [state => state.currencyTable],
  currencyTable => R.sort(diff, currencyTable)
);

const { actions, reducer } = currencyTableData;
export const { setCurrencyTable, updateCurrencyFavorite } = actions;
export const currencyTable = reducer;

const GET_CURRENCY_TABLE_DATA = "GET_CURRENCY_TABLE_DATA";
const UPDATE_CURRENCY_FAVORITE = "UPDATE_CURRENCY_FAVORITE";
export const getCurrencyTableDataAction = createAction(
  `${prefix}/${GET_CURRENCY_TABLE_DATA}`
);
export const updateCurrencyFavoriteAction = createAction(
  `${prefix}/${UPDATE_CURRENCY_FAVORITE}`
);

function* updateCurrencyFavoriteSaga(action) {
  yield put(updateCurrencyFavorite(action.payload));
}

function* getCurrencyTableDataSaga() {
  yield delay(2000);
  yield put(setCurrencyTable(defaultObj));
}

export function* watchCurrencyTable() {
  yield all([
    takeEvery(getCurrencyTableDataAction, getCurrencyTableDataSaga),
    takeEvery(updateCurrencyFavoriteAction, updateCurrencyFavoriteSaga)
  ]);
}
