export const diff = (a, b) => {
  const aa = new Date(a.favorite.updatedAt);
  const bb = new Date(b.favorite.updatedAt);
  // eslint-disable-next-line no-nested-ternary
  return aa > bb ? -1 : aa < bb ? 1 : 0;
};
