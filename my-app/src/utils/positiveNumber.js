export const isNumberPositive = number => {
  return Number(number) >= 0;
};
