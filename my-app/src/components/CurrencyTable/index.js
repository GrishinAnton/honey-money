import React, { useEffect } from "react";
import { Table, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {
  getCurrencyTableDataAction,
  updateCurrencyFavoriteAction,
  sortedCurrencyTableData
} from "../../features/currencyTable/ducks";
import { isNumberPositive } from "../../utils/positiveNumber";

const CurrencyTable = () => {
  const tableData = useSelector(sortedCurrencyTableData);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCurrencyTableDataAction());
  }, [dispatch]);

  const titles = ["Название", "Код", "Цена", "Статистика", "Избранное"];

  const onFavoriteHandler = ({ target }, id) => {
    dispatch(
      updateCurrencyFavoriteAction({
        status: target.checked,
        id,
        updatedAt: new Date()
      })
    );
  };

  const NumberContainer = styled.td`
    background-color: ${props =>
      props.positive ? "lightgreen" : "lightcoral"};
  `;

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          {titles.map(item => (
            <th key={item}>{item}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {tableData &&
          tableData.map(item => (
            <tr key={item.id}>
              <td>{item.title}</td>
              <td>{item.code}</td>
              <td>{item.price}</td>
              <NumberContainer positive={isNumberPositive(item.percent)}>
                <span>{`${item.percent} %`}</span>
              </NumberContainer>
              <td>
                <Form.Check
                  checked={item.favorite.status}
                  onChange={e => onFavoriteHandler(e, item.id)}
                  type="checkbox"
                />
              </td>
            </tr>
          ))}
      </tbody>
    </Table>
  );
};

export default CurrencyTable;
