import { all, fork } from "redux-saga/effects";
import { watchCurrencyTable } from "./features/currencyTable/ducks";

// eslint-disable-next-line require-yield
function* initSaga() {
  // eslint-disable-next-line no-console
  console.log("initSaga");
}

export function* rootSaga() {
  yield all([fork(initSaga), fork(watchCurrencyTable)]);
}
