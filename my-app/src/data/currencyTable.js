export const defaultObj = [
  {
    id: "1",
    img: "tartata",
    title: "Булорусский рубль",
    code: "RUR",
    price: 65.11,
    favorite: {
      status: false,
      updatedAt: new Date()
    },
    percent: 10.1
  },
  {
    id: "2",
    img: "tartata",
    title: "Армянская бубна",
    code: "AR",
    price: 65.11,
    favorite: {
      status: false,
      updatedAt: new Date()
    },
    percent: -10.1
  }
];
