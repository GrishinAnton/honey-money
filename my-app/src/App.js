import React from "react";
// eslint-disable-next-line import/no-unresolved
import "styles/App.css";
import { Container } from "react-bootstrap";
import CurrencyTable from "./components/CurrencyTable";

const App = () => (
  <Container>
    <h1>Курсы валют</h1>
    <CurrencyTable />
  </Container>
);

export default App;
