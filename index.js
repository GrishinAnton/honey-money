const express = require('express');
const {ApolloServer, gql} = require('apollo-server-express');
const {MongoClient} = require('mongodb');
const {readFileSync} = require('fs');
const expressPlayground = require('graphql-playground-middleware-express').default
const fetch = require('node-fetch');
const convert = require('xml-js');

require('dotenv').config()

const DB_HOST = `mongodb://${process.env.dbuser}:${process.env.dbpassword}@ds255728.mlab.com:55728/honey-money`


const typeDefs = gql`
    type Query {
        hello: String
    }    
`

const resolvers = {
    Query: {
        hello: () => 'Hello World'
    }
}

async function start () {
    const app = express()
    let db

    try {
        const client = await MongoClient.connect(DB_HOST, { useUnifiedTopology: true, useNewUrlParser: true})
        db = client.db()
    } catch(e) {
        console.log(e);
        process.exit(1)   
    }

    const server = new ApolloServer({
        typeDefs,
        resolvers,
        context: async () => {
            return {db}
        }
    })
    
    server.applyMiddleware({ app })
    
    app.get('/playground', expressPlayground({endpoint: '/graphql'}))
    
    app.get('/', (req, res) => {
        res.end('Hello World')
    })
    
    app.listen({port: 4000}, () => {
        console.log('server start');
    })


    const res = await fetch('http://www.cbr.ru/scripts/XML_daily.asp')
    const body = await res.textConverted()

    function nativeType(value) {
        var nValue = Number(value);
        if (!isNaN(nValue)) {
            return nValue;
        }
        var bValue = value.toLowerCase();
        if (bValue === 'true') {
            return true;
        } else if (bValue === 'false') {
            return false;
        }
        return value;
    }

    var removeJsonTextAttribute = function (value, parentElement) {
        try {
            var keyNo = Object.keys(parentElement._parent).length;
            var keyName = Object.keys(parentElement._parent)[keyNo - 1];
            parentElement._parent[keyName] = nativeType(value);
        } catch (e) { }
    }
    
    const convertConfig = { compact: true, spaces: 2, ignoreDeclaration: true, ignoreAttributes: true, textFn: removeJsonTextAttribute }
    const convertToJson = convert.xml2js(body, convertConfig)

    console.log(convertToJson.ValCurs.Valute)
}

start()